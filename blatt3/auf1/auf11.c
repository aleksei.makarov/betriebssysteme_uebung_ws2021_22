
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <signal.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <time.h>

int shmid1, shmid2, status;

int main() {
	srand (time(NULL));
	int *ppint, *pcint;
	shmid1 = shmget(IPC_PRIVATE, sizeof(int), IPC_CREAT | 0777);
	shmid2 = shmget(IPC_PRIVATE, sizeof(int), IPC_CREAT | 0777);
	ppint = (int *) shmat (shmid1, 0, 0);
	pcint = (int *) shmat (shmid2, 0, 0);
	*ppint = 0; 
	if (fork() == 0) {
		while (*ppint == 0) {
		} // An dieser Stelle wartet Child-Prozess bis *ppint != 0 wird
		printf("Child: a random value = %d\n", *pcint);
		*ppint = 0;
		exit(0);
	}
	*pcint = rand();
	*ppint = 1;		
	wait(&status);
	return 0;
}
