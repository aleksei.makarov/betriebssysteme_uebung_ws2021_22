# Betriebssysteme, Übung 03

### Aufgabe 1

3. Der Befehl ```ipcs``` zeigt die Information ueber Kommunikation zwischen Prozessen. Es wird die Information ueber 3 Ressourcen angezeigt: shared memory segments, message queues, semaphore arrays. Es gibt gewisse Optionen ```-a``` - detallierte Informationen, ```-m``` gibt Informationen ueber lebendige shared memory segments.\
Man koennte als ein Beispiel ```ipcs``` ausfuehren und dann ```./auf11``` aus dem Blatt 3 starten. Weiterhin kann man weitere Informationen ueber entstandene shmid's mit ```ipcs -m -i shmid_id (48, 49 bei mir)``` kriegen.\
\
```ipcrm``` eliminiert gewisse IPC Ressourcen. Dazu gibt es auch einige Optionen, wie zum Beispiel ```-m <shmid>``` - entfernt einen shared memory Segment nach ID oder ```-M <key>``` - entfernt nach dem Schluessel, also um oberen shared memory Segment zu entfernen, koennte man ```ipcrm -m 48``` eingeben.
