#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <signal.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <string.h>

#define LEN 1024

const int size = (sizeof(char)*LEN);
char *pint;
int ind;

void handler(int sig1);


int main() {
	signal(SIGUSR1, handler);
	printf("Read ist begonnen\n");
	int shmid, sig;
	pid_t ppid;
	long i = 100;
	while (shmget(i, size, IPC_CREAT | 0777) == (-1))
		i++;
	shmid = shmget(i, size, IPC_CREAT | 0777);
	pint = (char *) shmat (shmid, 0, 0);
	ppid = getpid();
	printf("The read-prcoess id and key are: %d, %ld\n", ppid, i);
	//system("./writemy");
	while(1) {
		//printf("Hello after writemy again!\n");
		ind = 0;
		//raise(SIGUSR1);
		pause();
		if (ind == 1) {
			//printf("ind = \n");
			printf("%s", pint);
		}
	}
	//handler()
	//printf("%s\n", pint);
	//pause();
	return 0;
}

void handler(int sig1) {
	if (sig1 == SIGUSR1) {
		//printf("Hello from signal\n");
		//printf("%s\n", pint);
		ind = 1;
	}
	signal(SIGUSR1, handler);
}

