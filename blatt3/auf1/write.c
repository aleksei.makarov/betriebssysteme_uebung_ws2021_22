#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <signal.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <string.h>

#define LEN 1024
#define P 5
#define L 80
#define MAX_LINE_LENGTH 80

int help1(char line[], char* params[], char delim[]);

const int size = (sizeof(char)*LEN);
int status;

int main() {
	printf("Write-Process ist begonnen\n");
	int shmid, id, death, key, q = 0;
	char line[L], linefile[MAX_LINE_LENGTH] = {0};
	char *params[P];
	char delim[] = " ";
	char *pcint;
	printf("Geben Sie PID von Lese, Schluessel und Dateinamen, durch space getrennt:\n");
	help1(line, params, delim);
	id = atoi(params[0]); // mach pid zum integer
	key = atoi(params[1]);
	shmid = shmget(key, size, IPC_CREAT | 0777);
	pcint = (char *) shmat (shmid, 0, 0);
	
	FILE *file = fopen(params[2], "r");
	if (!file) {
		printf("Datei nicht gefunden!\n");
		return 0;
	}
	while (fgets(linefile, MAX_LINE_LENGTH, file)) {
        	/* Print each line */
       	strcpy(pcint, linefile);
		kill(id, SIGUSR1);
		//waitpid(id, 0, 0);
		sleep (0.001);
		//printf("kill gesendet\n");
	}
	kill(id, SIGTERM);
	printf("now it's the real end\n");
	
	return 0;
}

int help1(char line[], char* params[], char delim[]) {
int i = 0;
  while(i < L && (line[i] = getchar()) != '\n')
    i++;
  
  if (line[i] != '\n')
    while(getchar() != '\n');
  line[i] = '\0';
  
  i = 0;
  char *p = strtok(line, delim);
  while(i < P && (params[i] = p) != NULL) {
    p = strtok(NULL, delim);
    i++;
  }
  params[i] = NULL;
  
  return 0;
}
