#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>

int main(int argc, char* argv[]) {
	char buffer[1];
	if(argc != 2) {
		printf("Wrong number of arguments, need only a pipe name\n");
		return 0;
	}
	int fp = open(argv[1], O_RDONLY); // man open benutzen um alle Packete hinzufuegen
	while(1) {
		while (read(fp, buffer, 1) != NULL)
			printf("%c", buffer[0]); // Immer nach einem einzelnen char lesen
	}
	return 1;
}
/*
Einen pipe zu schaffen: mknod pipe1 p
In einen pipe zu schreiben: echo msg0 > pipe1
*/
