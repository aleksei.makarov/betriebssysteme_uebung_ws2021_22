#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

#define L 80

int status;
char str2[L];

int main(int argc, char* argv[]) {
	char str1[L];
	int fd[2];
	int i;
	if(argc != 2) {
		printf("Wrong number of arguments\n");
		return 0;
	}
	pipe(fd);
	if (fork() == 0) {
		close(fd[1]);
		sleep(1); // Man braucht diese Zeile um genug Zeit fuer Child zum Lesen zu geben 
		read(fd[0], str2, L); // Z.B. setze L = 100, dann ist merkbar, dass nicht alles ausgegeben wird.
 		printf("%s\n", str2);
		exit(0);
	}
	for(i = 0; i < L-1 && argv[1][i] != '\0'; i++)
		str1[i] = argv[1][i];
    
	for(; i < L; i++)
		str1[i] = '\0';
	close(fd[0]);
	for(i = 0; i < L; i++)
		write(fd[1], &(str1[i]), 1); // Mehrere write-Befehle wurden verwendet, ansonsten einfach ein
	wait(&status); //			  einzelnes Array verwenden
	return 1;
}

