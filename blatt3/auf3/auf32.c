#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/fcntl.h>
#include <string.h>

#define L 80
int status;

char *addchar(char *str, char ch) {
	int length = strlen(str);
	char *str2;
	str2 = (char*) calloc(length + strlen("\n") + 1, sizeof(char));
	if (str2 == NULL) {
		printf("Fehler, beim calloc-Aufruf (memory allocation)");
		return NULL;
	}
	strcat(str2, str);
	str[length] = ch;
	str[length+1] = '\0';
	return str;
}

char *del_space(char *str) {
	int i = 0, j = 0;
	while (str[i] != '\0') {
		if (str[i] != ' ')
			str[j++] = str[i];
		i++;
	}
	str[j] = '\0';
	return str;
}

int main(int argc, char *argv[]) {
	if(argc != 2) {
		printf("Wrong number of arguments!\n");
		return 1;
	}
	char buffer[L];
	char *str1 = addchar(argv[1], '\n');
	del_space(str1); // Hier wird bei str1 alle Spaces entfernt und '\n' am Ende gesetzt
	printf("%s\n", str1);
	int downstream[2], upstream[2];
	pipe(downstream);
	pipe(upstream); // 2 downstream, upstream pipes
	
	write(downstream[1], str1, strlen(str1));
	if(fork() == 0) {
		close(downstream[1]); // downstream braucht nichts zu schreiben hier
		close(upstream[0]); //upstream braucht nichts zu lesen hier
		close(STDOUT_FILENO); //hier schliesst man beide Dateideskriptoren wie in Voraussetzung
		close(STDIN_FILENO);
		dup2(downstream[0], STDIN_FILENO); //dupliziert die Dateideskriptoren wie im Blatt
		dup2(upstream[1], STDOUT_FILENO);
		int err = execl("/usr/bin/bc", "bc", NULL);
		exit(status);
	}
	close(downstream[0]);
	close(upstream[1]);  
	read(upstream[0], buffer, L);
	char str2[L];
	strcpy(str2, buffer);
	printf("%s", str2);
	return 0;
}
