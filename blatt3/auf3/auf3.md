# Betriebssysteme, Übung 03

### Aufgabe 3

1. Der Systemaufruf ```dup2()``` erlaubt den Datei-Deskriptor umzutauschen, also zum Beispiel den Inhalt durch Umleitung zu aendern. Die Makros ```STDIN_FILENO, STDOUT_FILENO, STDERR_FILENO``` leiten den Deskriptor entsprechenderweise auf Standard-Eingabe, -Ausgabe, oder Error-message.
