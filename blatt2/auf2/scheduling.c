#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<limits.h>
#include<stdbool.h>

typedef struct {
	int id;
	int arrival;
	int burst;
	int prio;
}process;

int compareArrival(const void* a, const void* b);
int compareBurst(const void* a, const void* b);
int comparePrio(const void* a, const void* b);

int main() {
	int size = 5;
	FILE* f = fopen("input", "r");
	process processes[size];

	//READ FILE	
	int line[3];
	for(int i = 0; i<size; i++) {
		fscanf(f, "%d;%d;%d\n", &line[0], &line[1], &line[2]);
		processes[i].id = i;
		processes[i].arrival = line[0];
		processes[i].burst = line[1];
		processes[i].prio = line[2];
	}	
	fclose(f);
	
	//SCHEDULINGS
	int sliceTicks = 4;
	process temp[size];
	int ticks;
	int turnaroundTimes[size];
	int waitingTimes[size];
	double avgTurnaround;
	double avgWaiting;
	int sum;

	//Sort processes by arrival time
	qsort(processes, size, sizeof(process), compareArrival);
	
	//###################################FCFS####################################

	for(int i = 0; i<size; i++) {
		temp[i] = processes[i]; 
	}
	
	printf("#####FCFS#####\n");
	printf("Process\tArrival\tBurst\tWaiting\tTurnaround\n");
	
	ticks = 0;
	for(int i = 0; i<size; i++) {
		int waiting;
		if(ticks > temp[i].arrival) waiting = ticks - temp[i].arrival;
		else waiting = 0;
		waitingTimes[i] = waiting;
		for(ticks; ticks < temp[i].arrival; ticks++);
		for(int j = 0; j<temp[i].burst; j++) {
			ticks++;
		}
		int turnaround = temp[i].burst + waiting;
		turnaroundTimes[i] = turnaround;
		printf(
			"P%d\t%d\t%d\t%d\t%d\n",
			temp[i].id,
			temp[i].arrival,
			temp[i].burst,
			waiting,
			turnaround
		);
	}
	sum = 0;
	for(int i = 0; i<size; i++) {
		sum += turnaroundTimes[i];
	}
	avgTurnaround = (double)sum/(double)size;
	sum = 0;
	for(int i = 0; i<size; i++) {
		sum += waitingTimes[i];
	}
	avgWaiting = (double)sum/(double)size;
	printf("Avg Waiting: %.2f\n", avgWaiting);
	printf("Avg Turnaround: %.2f\n", avgTurnaround);
	printf("\n");
	
	
	//###################################SJF####################################
	printf("#####SJF#####\n");
	printf("Process\tArrival\tBurst\tWaiting\tTurnaround\n");
	
	for(int i = 0; i<size; i++) {
		temp[i] = processes[i]; 
	}
	
	int count = 0;
	ticks = 0;
	while(count < size) {
		int shortestJobIndex = -1;
		int shortestJobTime = INT_MAX;
		for(int i = 0; i < size; i++) {
			if(temp[i].arrival <= ticks && temp[i].burst < shortestJobTime && temp[i].id != -1) {
				shortestJobTime = temp[i].burst;
				shortestJobIndex = i;
			}
		}
		
		if(shortestJobIndex != -1) {
			int waiting;
			if(ticks > temp[shortestJobIndex].arrival) {
				waiting = ticks - temp[shortestJobIndex].arrival;
			}
			else {
				waiting = 0;
			}
			waitingTimes[shortestJobIndex] = waiting;
			int turnaround = temp[shortestJobIndex].burst + waiting;
			turnaroundTimes[shortestJobIndex] = turnaround;
			printf(
				"P%d\t%d\t%d\t%d\t%d\n",
				temp[shortestJobIndex].id,
				temp[shortestJobIndex].arrival,
				temp[shortestJobIndex].burst,
				waiting,
				turnaround
			);
			ticks += temp[shortestJobIndex].burst;
			count++;
			temp[shortestJobIndex].id = -1;
		} else {
			ticks++;
		}
	}
	sum = 0;
	for(int i = 0; i<size; i++) {
		sum += turnaroundTimes[i];
	}
	avgTurnaround = (double)sum/(double)size;
	sum = 0;
	for(int i = 0; i<size; i++) {
		sum += waitingTimes[i];
	}
	avgWaiting = (double)sum/(double)size;
	printf("Avg Waiting: %.2f\n", avgWaiting);
	printf("Avg Turnaround: %.2f\n", avgTurnaround);
	printf("\n");
	
	//###################################SRTF####################################
	printf("#####SRTF#####\n");
	printf("Process\tArrival\tBurst\tWaiting\tTurnaround\n");
	
	for(int i = 0; i<size; i++) {
		temp[i] = processes[i]; 
	}
	
	int startTicks[size];
	int endTicks[size];
	
	int currentIndex = -1;
	bool done = false;
	ticks = 0;
	while(!done) {
		int shortestJobIndex = -1;
		int shortestJobTime = INT_MAX;
		int burstLeft = false;
		for(int i = 0; i < size; i++) {
			if(!burstLeft) {		
				burstLeft = temp[i].burst > 0;
			}
			if(temp[i].arrival <= ticks && temp[i].burst < shortestJobTime && temp[i].burst > 0) {
				shortestJobTime = temp[i].burst;
				shortestJobIndex = i;
			}
		}
		
		if(!burstLeft) {
			endTicks[currentIndex] = ticks;
		 	for(int i = 0; i<size; i++) {
		 		int turnaround = endTicks[i] - processes[i].arrival;
		 		int waiting = turnaround - processes[i].burst;
		 		waitingTimes[i] = waiting;
		 		turnaroundTimes[i] = turnaround;
		 		printf(
				"P%d\t%d\t%d\t%d\t%d\n",
				processes[i].id,
				processes[i].arrival,
				processes[i].burst,
				waiting,
				turnaround
			);
		 	}
			done = true;
			break;
		}
		
		if(currentIndex == -1) {
			currentIndex = shortestJobIndex;
		} else if(currentIndex == shortestJobIndex) {
			temp[currentIndex].burst--;
			ticks++;
		} else {
			if(temp[currentIndex].burst == 0) {
				endTicks[currentIndex] = ticks;
			}
			currentIndex = shortestJobIndex;
		}
	}
	sum = 0;
	for(int i = 0; i<size; i++) {
		sum += turnaroundTimes[i];
	}
	avgTurnaround = (double)sum/(double)size;
	sum = 0;
	for(int i = 0; i<size; i++) {
		sum += waitingTimes[i];
	}
	avgWaiting = (double)sum/(double)size;
	printf("Avg Waiting: %.2f\n", avgWaiting);
	printf("Avg Turnaround: %.2f\n", avgTurnaround);
	printf("\n");
	
	
	//###################################ROUND-ROBIN####################################
	printf("#####ROUND-ROBIN#####\n");
	printf("Process\tArrival\tBurst\tWaiting\tTurnaround\n");
	
	for(int i = 0; i<size; i++) {
		temp[i] = processes[i]; 
	}
	
	currentIndex = -1;
	ticks = 0;
	done = false;
	while(!done) {
		if(currentIndex == -1) {
			currentIndex = 0;
			startTicks[currentIndex] = ticks;
		}
		if(temp[currentIndex].burst < sliceTicks) {
			ticks += temp[currentIndex].burst;
			temp[currentIndex].burst = 0;
		} else {
			ticks += sliceTicks;
			temp[currentIndex].burst -= sliceTicks;
		}
		if(temp[currentIndex].burst == 0) {
			endTicks[currentIndex] = ticks;
		}
		int tempCurrentIndex = currentIndex;
		for(int i = 0; i<size; i++) {
			int nextIndex = (currentIndex+i+1)%size;
			if(temp[nextIndex].arrival <= ticks && temp[nextIndex].burst > 0) {
				currentIndex = nextIndex;
				break;
			}
		}
		if(tempCurrentIndex == currentIndex) done = true;
	}
	for(int i = 0; i<size; i++) {
 		int turnaround = endTicks[i] - processes[i].arrival;
 		int waiting = turnaround - processes[i].burst;
 		waitingTimes[i] = waiting;
 		turnaroundTimes[i] = turnaround;
 		printf(
			"P%d\t%d\t%d\t%d\t%d\n",
			processes[i].id,
			processes[i].arrival,
			processes[i].burst,
			waiting,
			turnaround
		);
 	}
 	sum = 0;
	for(int i = 0; i<size; i++) {
		sum += turnaroundTimes[i];
	}
	avgTurnaround = (double)sum/(double)size;
	sum = 0;
	for(int i = 0; i<size; i++) {
		sum += waitingTimes[i];
	}
	avgWaiting = (double)sum/(double)size;
	printf("Avg Waiting: %.2f\n", avgWaiting);
	printf("Avg Turnaround: %.2f\n", avgTurnaround);
	printf("\n");
	
	
	//###################################ROUND-ROBIN-PRIO####################################
	printf("#####ROUND-ROBIN-PRIO#####\n");
	printf("Process\tArrival\tBurst\tWaiting\tTurnaround\n");
	
	for(int i = 0; i<size; i++) {
		temp[i] = processes[i]; 
	}
	
	currentIndex = -1;
	ticks = 0;
	done = false;
	while(!done) {
		if(currentIndex == -1) {
			currentIndex = 0;
			startTicks[currentIndex] = ticks;
		}
		if(temp[currentIndex].burst < sliceTicks) {
			ticks += temp[currentIndex].burst;
			temp[currentIndex].burst = 0;
		} else {
			ticks += sliceTicks;
			temp[currentIndex].burst -= sliceTicks;
		}
		if(temp[currentIndex].burst == 0) {
			endTicks[currentIndex] = ticks;
		}
		int tempCurrentIndex = currentIndex;
		int minPrio = 0;
		for(int i = 0; i<size; i++) {
			if(temp[i].arrival <= ticks && temp[i].burst > 0) {
				if(temp[i].prio > minPrio) {
					minPrio = temp[i].prio;			
				}
			}
		}
		for(int i = 0; i<size; i++) {
			int nextIndex = (currentIndex+i+1)%size;
			if(temp[nextIndex].arrival <= ticks && temp[nextIndex].burst > 0 && temp[nextIndex].prio == minPrio) {
				currentIndex = nextIndex;
				break;
			}
		}
		if(tempCurrentIndex == currentIndex && temp[currentIndex].burst == 0) done = true;
	}
	for(int i = 0; i<size; i++) {
 		int turnaround = endTicks[i] - processes[i].arrival;
 		int waiting = turnaround - processes[i].burst;
 		waitingTimes[i] = waiting;
 		turnaroundTimes[i] = turnaround;
 		printf(
			"P%d\t%d\t%d\t%d\t%d\n",
			processes[i].id,
			processes[i].arrival,
			processes[i].burst,
			waiting,
			turnaround
		);
 	}
 	sum = 0;
	for(int i = 0; i<size; i++) {
		sum += turnaroundTimes[i];
	}
	avgTurnaround = (double)sum/(double)size;
	sum = 0;
	for(int i = 0; i<size; i++) {
		sum += waitingTimes[i];
	}
	avgWaiting = (double)sum/(double)size;
	printf("Avg Waiting: %.2f\n", avgWaiting);
	printf("Avg Turnaround: %.2f\n", avgTurnaround);
	printf("\n");
	
}

int compareArrival(const void* a, const void* b)
{
     process processA = * ((process*)a);
     process processB = * ((process*)b);

     if(processA.arrival == processB.arrival) return 0;
     else if(processA.arrival < processB.arrival) return -1;
     else return 1;
}

