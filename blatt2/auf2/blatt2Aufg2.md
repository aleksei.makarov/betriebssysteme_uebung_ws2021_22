## Aufgabe 2.1

Code in der Datei "scheduling.c", input in der Datei "input", vom Programm generierter output in der Datei "output.txt"

## Aufgabe 2.2

Die Überlegung muss in der Praxis nicht beachtet werden, da die Prozesse ja immer in einer Art Warteschlange eingetragen werden, bevor der Scheduler sie verarbeitet. In einer durch einen Computer modelliertern Warteschlange ist es nicht möglich, dass zwei Prozesse sich einen Platz teilen, sprich, der Fall wird schon vorher von der Komponente, die die Prozesse in die Warteschlange einträgt, bzw die Warteschlange verwaltet, abgefangen. Abgesehen davon ist die Wahrscheinlichkeit, dass zwei Prozesse auf die Millisekunde genau gleichzeitig eintreffen so gering, dass sie vernachlässigt werden kann.
