#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include<unistd.h>

// Kompilieren durch gcc -pthread -o ue2-a1 ue2-a1.c
// Ausfuehren wie ueblic ./ue2-a1
// Anzahl der Threads 10
#define NUMTHRDS 10
// Feldgroesse pro Thread
#define CHUNKSIZE 10000

int *arr; 
long sum=0;
pthread_mutex_t mutex1; //An dieser Stelle wird mutex angelegt

/*************************************************************/
void *gausswasbetteratthis(void *threadid)
{
	pthread_mutex_lock(&mutex1);
  long start = (long)threadid*CHUNKSIZE;
  long end = start+CHUNKSIZE-1;

  for (long i=start; i<=end ; i++){ 
    sum += arr[i];
  }
  	pthread_mutex_unlock(&mutex1);

  pthread_exit((void*) 0);
}

/*************************************************************/
int main()
{
  long i;
  void* status;
  pthread_t threads[NUMTHRDS]; //hier wird ein array gemacht
  arr = (int*) malloc (CHUNKSIZE*NUMTHRDS*sizeof(int));

  for (i=0; i<CHUNKSIZE*NUMTHRDS; i++)
    arr[i] = i+1;
	
	  pthread_mutex_init(&mutex1, NULL);
  //Create 10 Threads for summing up numbers.
  for(i=0; i<NUMTHRDS; i++) 
    pthread_create(&threads[i], NULL, gausswasbetteratthis, (void *)i); 


  for(i=0; i<NUMTHRDS; i++)
    pthread_join(threads[i], &status);
	
	pthread_mutex_destroy(&mutex1);
  printf ("Summe der ersten %d Zahlen ist %li\n", NUMTHRDS*CHUNKSIZE, sum);
  free (arr);
  pthread_exit(NULL);
}   
/*************************************************************/
