#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

void *print_hello(void *threadid) {
	printf("Hallo von Thread, pthread ID - %ld\n", pthread_self());
	return 0;
}

int main() {
	pthread_t threads[10];
	int i;
 	for (i = 0; i < 10; i++)
		pthread_create(&threads[i], NULL, print_hello, NULL);
	for (i = 0; i < 10; i++)
		pthread_join(threads[i], NULL);
	return 0;
}
