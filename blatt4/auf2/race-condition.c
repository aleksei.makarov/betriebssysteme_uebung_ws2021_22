#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
/* Kompilieren mit der Option -pthread
   Fuer count_max = 5000 ist die Race-Condition merkbar.
*/
volatile long int in = 0;

void *increase(void *count_max) {
	unsigned long int nextvar;
	for(unsigned long int i = 0; i < (unsigned long int) count_max; i++) {
		nextvar = in;
		nextvar++;
		in = nextvar;
	}
	return NULL;
}

int main(int argc, char *argv[]) {
	if(argc != 2) {
		printf("Wrong number of arguments. count_max is expected!\n");
		return 0;
	}
	unsigned long int count_max = atol(argv[1]);
	pthread_t th1, th2;
	pthread_create(&th1, NULL, increase, (void*) count_max);
	pthread_create(&th2, NULL, increase, (void*) count_max);
	
	pthread_join(th1, NULL);
	pthread_join(th2, NULL);
	printf("In = %ld\n", in);
	return 1;
}

