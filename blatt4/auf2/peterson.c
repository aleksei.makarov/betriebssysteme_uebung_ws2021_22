#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <pthread.h>

volatile long int in = 0;
volatile long int count_max;
volatile bool flag[2] = {false, false}; // Glovale Variablen zur Synchronisation
volatile int turn;

// Um die Variable in zu vergroessern, verwenden wir Peterson-Algorithmus von der Seite 94 
void *increase(void *thrd_id) {
	int thread_id = *((int *)thrd_id); // Umwandlung zum Zeiger auf int (Uebergegebener thread ID)
	int other_id = (thread_id + 1) % 2;
	printf("%ld\n", count_max);
	while (1) {
		flag[thread_id] = true;
		asm volatile("mfence" ::: "memory");
		turn = other_id;
		asm volatile("mfence" ::: "memory");
		while (flag[other_id] == true && turn == other_id) {
		}
		asm volatile("mfence" ::: "memory");
		
		long int i = 0, nextvar = in;
		//long int next_free_slot = in;
		for (i = 0; i < count_max; i++) {
			nextvar++;
			in = nextvar;
		}
		flag[thread_id] = false;
		asm volatile("mfence" ::: "memory");
		pthread_exit((void*) 0); // Wurde aus dem Blatt 2 ue2-a1.c genommen
	}
}

int main(int argc, char *argv[]) {
	pthread_t threads[2];
	if(argc != 2) {
		printf("Please supply an count_max!\n");
		return 0;
	}
	count_max = atol(argv[1]);
	int thread_id[2] = {0, 1};
	pthread_create(&threads[0], NULL, increase, (void *) &(thread_id[0]));
	pthread_create(&threads[1], NULL, increase, (void *) &(thread_id[1]));

	pthread_join(threads[0], NULL);
	pthread_join(threads[1], NULL);
	printf("In: %ld\n", in);
	return 1;
}
/* Antwort auf Teil 4 der Aufgabe 2:
   Ich glaube, dass es keinen Unterschied macht, welchen Wert wir fuer turn auswaehlen und mit welchem Wert wir
   turn auch vergleichen, weil vom kritischen Abschnitt turn nur einen Wert annimt, und zwar 1 oder 2, also
   einen der beiden.   
*/

