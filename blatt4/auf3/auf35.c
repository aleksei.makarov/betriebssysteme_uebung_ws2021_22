#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>

#define L 4 // hier ist die Anzahl von threads gestellt

sem_t sem[L]; // 4 Semaphore 
volatile long int in;
volatile unsigned long int count_max;

void *increase(void *threadid) {
	unsigned long int nextvar;
	int thread_id = *((int *)threadid);
	unsigned long int i;
	for(i = 0; i < (unsigned long int) count_max; i++) {
		sem_wait(&sem[thread_id]); // sem[i] blockiert
		printf("Kritischer Abschnitt vom thread %d\n", thread_id);
		nextvar = in;
		nextvar++;
		in = nextvar;
		sem_post(&sem[(thread_id + 1) % L]); // sem[(i + 1) % L freigesetzt]
	}
	return NULL;
}

int main(int argc, char *argv[]) {
	if(argc != 2) {
		printf("Wrong number of arguments. count_max is expected!\n");
		return 0;
	}
	count_max = atol(argv[1]);
	pthread_t threads[L];
	int thread_id[L];
	int i = 0;
	in = 0;
	sem_init(&sem[0], 0, 1);
	for(i = 0; i < L; i++) {
		if (i != 0)
			sem_init(&sem[i], 0, 0);
		thread_id[i] = i;
		pthread_create(&threads[i], NULL, increase, &thread_id[i]);
	}
	
	for(i = 0; i < L; i++)
		pthread_join(threads[i], NULL);
	
	printf("in = %ld\n", in);
	for(int i = 0; i < L; i++)
		sem_destroy (&sem[i]);
	return 1;
}
