#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>

#define L 4 // hier wird ein Semaphor verwendet

sem_t sem1;
volatile long int in;
volatile unsigned long int count_max;

void *increase(void *threadid) {
	unsigned long int nextvar;
	int thread_id = *((int *)threadid);
	unsigned long int i;
	for(i = 0; i < (unsigned long int) count_max; i++) {
		sem_wait(&sem1);
		printf("Kritischer Abschnitt vom thread %d\n", thread_id);
		nextvar = in;
		nextvar++;
		in = nextvar;
		sem_post(&sem1);
	}
	return NULL;
}

int main(int argc, char *argv[]) {
	if(argc != 2) {
		printf("Wrong number of arguments. count_max is expected!\n");
		return 0;
	}
	count_max = atol(argv[1]);
	pthread_t threads[L];
	int thread_id[L];
	int i = 0;
	in = 0;
	sem_init(&sem1, 0, 1);
	for(i = 0; i < L; i++) {
		thread_id[i] = i;
		pthread_create(&threads[i], NULL, increase, &thread_id[i]);
	}
	for(i = 0; i < L; i++)
		pthread_join(threads[i], NULL);
	
	printf("in = %ld\n", in);
	sem_destroy (&sem1);
	return 1;
}

