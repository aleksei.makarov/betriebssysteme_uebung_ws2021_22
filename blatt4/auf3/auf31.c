#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#define L 4 // 4 threads greifen auf die globale Variable in zu
volatile long int in;
volatile unsigned long int count_max;

void *increase(void *threadid) {
	unsigned long int nextvar;
	int thread_id = *((int *)threadid);
	for(unsigned long int i = 0; i < (unsigned long int) count_max; i++) {
		printf("Kritischer Abschnitt vom thread %d\n", thread_id);
		nextvar = in;
		nextvar++;
		in = nextvar;
	}
	return NULL;
}

int main(int argc, char *argv[]) {
	if(argc != 2) {
		printf("Wrong number of arguments. count_max is expected!\n");
		return 0;
	}
	count_max = atol(argv[1]);
	pthread_t thread[L];
	int thread_id[L];
	int i = 0;
	in = 0;
	for(i = 0; i < L; i++) {
		thread_id[i] = i;
		pthread_create(&thread[i], NULL, increase, &thread_id[i]);
	}
	for(i = 0; i < L; i++)
		pthread_join(thread[i], NULL);
	
	printf("in = %ld\n", in);
	return 1;
}

