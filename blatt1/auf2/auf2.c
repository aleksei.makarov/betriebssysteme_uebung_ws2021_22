#include <pthread.h>
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <signal.h>
#include <string.h>
#include <signal.h>

#define NUM_CHILDS 10 // N
#define L 80
#define P 10

int err, status = 0;

int reading(char line[], char* params[], char delim[], int mode);
int eqstr (char *s1, char *s2);

int main(int argc, char *argv[]) {
	char line[L];
	char line1[L];
	char* params[P];
	char delim[] = " ";
	int i, alive, filemode = 0, j;
	FILE* file;
	/*for(i = 0; i != NUM_CHILDS; i++)
		params[i] = NULL;*/
	pid_t childpid, childpid1, ppid, childpid2;
	int pids[NUM_CHILDS], livingpids[NUM_CHILDS];
	do {	
		if (argc >= 3 && filemode == 0 && eqstr("-f", argv[2])) {
			filemode = 1;
			printf("Filemode\n");
			file = fopen(argv[1], "r");
		}
		if (filemode == 1) {
			if(!feof (file)) {
				if (fgets(line1, sizeof(line1), file)) {
					printf("%s", line1);
					reading(line1, params, delim, 0);
				}
			}
			else {
				filemode = 2;
				fclose(file);
			}
		}
		if (filemode != 1)
			reading(line, params, delim, 1);
		/*for(i = 0; i < NUM_CHILDS; i++) {
			if (params[i] == NULL)
				printf("NULL\n");
			else
				printf("params[%d] = %s\n", i, params[i]);
		}*/
		i = 0;
		if (params[0] == NULL) {
			printf("continue here\n");
			continue;
		}
		if (eqstr("quit", params[0])) {
			printf("quit here\n");
			//return 0;
			break;
		}
		if (eqstr("childs", params[0])) {
			printf("childs here\n");
			for (i = 0; pids[i] != 0; i++) {
				if (kill(pids[i], 0) == -1)
					livingpids[i] = 0;
				else
					livingpids[i] = 1;
			}
			for (i = 0; i < NUM_CHILDS; i++)
				printf("Process PID = %d, status = %d\n", pids[i], livingpids[i]);
			continue;
		}
		else {
			printf("some child\n");
			for(i = 0; params[i] != NULL; i++) {
				printf("%d -> ,%s,\n", i, params[i]);
			}
			i--;
			int len = strlen(params[i]);
			params[i][len - 1] = '\0';
			childpid = fork();
			if (eqstr("&", params[i]))  {
				printf("With kaufand\n");
				printf("%d\n", (eqstr("&", params[i])));
				if (childpid == 0) {
					params[i] = NULL;
					setpgid(0, 0);
					err = execvp(params[0], params);
					if(err == -1)
						printf("Wrong argument: %s\n", params[0]);
					exit(0);
				}
				for(i = 0; pids[i] != 0 && i < NUM_CHILDS; i++) {
					;
				}
				i = i % NUM_CHILDS;
				pids[i] = childpid;
				waitpid(-1, &status, WNOHANG);
			}
			//else if (eqstr("&", params[i]) == 0){
			else {
				printf("Without kaufand\n");
				printf("%d\n", (eqstr("&", params[i])));
				if (childpid == 0) {
					execv(params[0], params);
					//execl("/usr/bin/bc", "bc", NULL);
					exit(0);
				}
				else {
					wait(&status);
				}
			}
		}
		for(i = 0; i != NUM_CHILDS; i++)
			params[i] = NULL;
		for(i = 0; line[i] != L; i++)
			line[i] = '\0';
		printf("------------------\n");
	} while(1);
	free(pids);
	return 0;
}

int reading(char line[], char* params[], char delim[], int mode) {
	int i = 0;
	if (mode) {
		while(i < L && (line[i] = getchar()) != '\n')
			i++;  
		if (line[i] != '\n') {
			while(getchar() != '\n');
				line[i] = '\0';
		}
		if (i == 0) {
			params[0] = NULL;
			return 0;
		}
	}
	else {
		while(i < L && (line[i]) != '\n')
			i++;  
		/*if (line[i] != '\n') {
			//while(getchar() != '\n');
			line[i] = '\0';
		}*/
		if (i == 0) {
			params[0] = NULL;
			return 0;
		}
	}
	i = 0;
	char *p = strtok(line, delim);
	while(i < P && (params[i] = p) != NULL) {
		p = strtok(NULL, delim);
		i++;
	}
	//params[i] = NULL;
	if (params[0] == NULL)
		printf("NULL\n");
	return 0;
}

int eqstr (char s1[], char s2[]) {
	int len = strlen(s1), i = 0;
	for(; i != len; i++) {
		if (s1[i] != s2[i]) {
			return 0;
		}
	}
	if (s1[i] == s2[i] && s2[i] == '\0')
		return 1;
}
