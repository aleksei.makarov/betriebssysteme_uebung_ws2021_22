
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <signal.h>

int status;

int main() {
	int i = 0;
	for (; i < 3; i++) {
		pid_t pid0 = fork();
		if (pid0 == 0) {
			printf("Zombie %d mit pid %d ist erzeugt\n", i, getpid());
			exit(0);
		}
	}
	while(1) {
	}
	wait(&status);
	return 0;
}
/*
./zombie liefert

Zombie 0 mit pid 7065 ist erzeugt
Zombie 1 mit pid 7066 ist erzeugt
Zombie 2 mit pid 7067 ist erzeugt

ps -A | grep zombie liefert

   7064 pts/1    00:00:14 zombie 
   7065 pts/1    00:00:00 zombie <defunct>
   7066 pts/1    00:00:00 zombie <defunct>
   7067 pts/1    00:00:00 zombie <defunct>
*/
