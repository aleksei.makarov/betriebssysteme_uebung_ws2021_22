# Betriebssysteme, Übung 01

### Aufgabe 1

2. Man fuehre die beiden Befehle ```pstree``` und ```ps -af```\
Mit ps -af sind folgende pids gegeben\
Parent pid = 6236\
Child pid = 6237\
\
Laut man pstree kann man pid als Argument uebergeben:
pstree 6236 -> uebung11───uebung11\
pstree 6237 -> uebung11\
pstree liefert folgende Ast: gnome-terminal-─┬─bash───uebung11───uebung11\

3. Nach dem Befehl kill 6236 lauft Child-Prozess weiter mit PID 6237, sein ppid = 790
