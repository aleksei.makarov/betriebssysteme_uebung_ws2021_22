
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>

int main() {
	pid_t pid0 = fork();
	if (pid0 == 0) {
		while (1) {
			pid_t pid1 = getpid(), pid2 = getppid(); 
			printf("Child pid = %d, ppid = %d\n", pid1, pid2);
		}
	}
	else {
		while (1) {
			pid_t pid1 = getpid();
			printf("Parent pid = %d\n", pid0);
		}
	}
	return 0;
}
